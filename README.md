Tutorial Utilizando Firebase Cloud Messaging no Ionic 2
===============
Aprenda como utilizar o FCM para enviar e receber notificações com Ionic 2 e os conceitos de comunicação indireta envolvidos nessa tecnologia.

## Introdução

Para entendermos melhor como o funcionamento do FCM, precisamos ver os conceitos básicos de sua estrutura como comunicação indireta e modelo publish/subscribe.

### Comunicação Indireta

É definida como uma comunicação entre entidades em um sistema distribuído através de um intermediário (middleware) e sem acoplamento direto entre o emissor e o receptor.

* Frequentemente usada em sistemas distribuídos onde são previstas alterações.
	Exemplo: ambientes móveis, onde os usuários podem se conectar e desconectar rapidamente da rede global.
* Utilizada para disseminação de eventos em sistemas distribuídos, em que os destinatários podem ser desconhecidos e propensos a mudanças.
	Exemplo: feeds de notícias em websites.

### Modelo Publish/Subscribe

O modelo de comunicação publish/subscribe é baseado na troca assíncrona de
mensagens, conhecidas como eventos. Ele consiste de um conjunto de clientes
que publicam esses eventos (produtores), os quais são encaminhados para clientes
que registraram interesse em recebê-los (consumidores).

<br>

<p align="center">
	<img src="https://sites.google.com/site/jvmtotal/_/rsrc/1472858755640/home/conceitos/servico-de-mensagem-java-jms/modelo-publicador-assinante-publish-subscribe/JMS03.jpg?height=176&width=400">
</p>

## Firebase Cloud Messaging
O Firebase Cloud Messaging (FCM) é uma solução de mensagens da Google que implementa o modelo Publicador/Assinante.

### Como funciona?
Uma implementação do FCM inclui dois componentes principais para envio e recebimento:
1. Um ambiente confiável como o Cloud Functions para Firebase ou um servidor de apps que você usa para criar, segmentar e enviar mensagens.
2. Um app cliente iOS, Android ou Web (JavaScript) que recebe as mensagens.

<p align="center">
	<img src="https://firebase.google.com/docs/cloud-messaging/images/messaging-overview.png?hl=pt-br">
</p>

## Ionic 2

Ionic é um framework para desenvolvimento de aplicações para dispositivos móveis que visa o desenvolvimento de apps híbridas e de rápido e fácil desenvolvimento.

Neste tutorial, precisamos que o leitor já tenha conhecimento e o código do aplicativo desenvolvido em Ionic 2.

Para mais informações acesse [Ionic Docs](https://ionicframework.com/docs/)

# Tutorial
## Utilizando Firebase Cloud Messaging no Ionic 2

### Configuração

1. **Acesse o [Firebase Console](https://console.firebase.google.com) e crie seu projeto**
![Example](./assets/img01.png)

2. **Configure o SDK para Android/iOS conforme a necessidade de seu projeto**
![Example](./assets/img02.png)

3. **Baixe o arquivo de configuração Firebase e coloque na pasta raiz do projeto Ionic**
>GoogleService-Info.plist para aplicativos iOS<br>gogle-services.json para aplicativos Android

4. **Instale o plugin FCM no projeto Ionic**
```
ionic cordova plugin add cordova-plugin-fcm
npm install --save @ionic-native/fcm
```
5. **[Adicione o plugin ao módulo App do Ionic](https://ionicframework.com/docs/native/#Add_Plugins_to_Your_App_Module)**

### Como utilizar

#### Inscrever em um tópico

```javascript
//FCMPlugin.subscribeToTopic( topic, successCallback(msg), errorCallback(err) );
//All devices are subscribed automatically to 'all' and 'ios' or 'android' topic respectively.
//Must match the following regular expression: "[a-zA-Z0-9-_.~%]{1,900}".
FCMPlugin.subscribeToTopic('topicExample');
```

#### Desinscrever de um tópico

```javascript
//FCMPlugin.unsubscribeFromTopic( topic, successCallback(msg), errorCallback(err) );
FCMPlugin.unsubscribeFromTopic('topicExample');
```

#### Receber mensagem de notificação

```javascript
//FCMPlugin.onNotification( onNotificationCallback(data), successCallback(msg), errorCallback(err) )
//Here you define your application behaviour based on the notification data.
FCMPlugin.onNotification(function(data){
    if(data.wasTapped){
      //Notification was received on device tray and tapped by the user.
      alert( JSON.stringify(data) );
    }else{
      //Notification was received in foreground. Maybe the user needs to be notified.
      alert( JSON.stringify(data) );
    }
});
```

Para mais informações acesse a [documentação oficial da biblioteca](https://github.com/fechanique/cordova-plugin-fcm)

## Referências

* [Indirect Communication - Netzprogrammierung](http://www.inf.fu-berlin.de/inst/ag-se/teaching/V-NETZPR-2015/07_Indirect_Communication%20-%20I.pdf)
* [Comunicação Indireta - Vitor Hazin da Rocha](https://pt.slideshare.net/victorhazin/aula-de-sistemas-distribudos-comunicao-indireta)
* [Modelo Publicador/Assinante - JVM Total](https://sites.google.com/site/jvmtotal/home/conceitos/servico-de-mensagem-java-jms/modelo-publicador-assinante-publish-subscribe)
* [Firebase Cloud Messaging](https://firebase.google.com/docs/cloud-messaging/?hl=pt-br)
* [Ionic Framework](https://ionicframework.com/framework)
* [Cordova Plugin FCM](https://github.com/fechanique/cordova-plugin-fcm)



